from flask import Flask, render_template
from flask import jsonify
from flask import request
import os.path

app = Flask(__name__)

def write_user(name,surname):
    if os.path.isfile('./users.txt'):
        with open('./users.txt', 'a') as file:
            file.write(f'{name}:{surname}\n')
    else:
        with open('./users.txt', 'w') as file:
            file.write(f'{name}:{surname}\n')
    return True

@app.route('/',methods=['GET'])
def home():
    return jsonify(
        firstname='oguzhalit',
        lastname='sak'
    )

@app.route('/info',methods=['GET'])
def info():
      return render_template('./info.html')
@app.route('/test',methods=['GET'])
def test():
    return 'hello'
@app.route('/whoami',methods=['GET','POST'])
def get_user():
    if request.method == 'GET':
        fname=request.args.get('firstname')
        lname=request.args.get('lastname')
        if fname is None or lname is None:
            return jsonify(
                error="null in request or unknown parameters"
            ), 400
    if request.method == 'POST':
        if request.json:
            fname=request.json['firstname']
            lname=request.json['lastname']
        else:
             fname=request.form.get('firstname')
             lname=request.form.get('lastname')
    write_user(fname,lname)
    return jsonify(firstname=fname,lastname=lname)
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
